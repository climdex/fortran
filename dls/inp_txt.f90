subroutine inp_txt
use COMM
implicit none
integer :: ID0,i
 character*200 :: ctmp
 real   :: data(0:13), data2(0:1)

 ID0=10
 call getID(ID0)   ! read in all the dls values ...
 open(ID0,file=dls_file)
 read(ID0,*) N_month
 read(ID0,*) Nlat
 read(ID0,*) ctmp
 read(ID0,*) ctmp
 allocate(dlss(Nlat,0:13),lat(Nlat))
 if(N_month==1) then
   print*,'NOTE: only annual dls value available ...'
   do i=1,Nlat
     read(ID0,*) data2
     dlss(i,0)=data2(0)
     dlss(i,1:13)=data2(1)
   enddo
 else
   do i=1,Nlat
     read(ID0,*) data
     dlss(i,:)=data
   enddo
 endif

 lat=dlss(:,0)
 read(ID0,*) Nlon
 allocate(lon(Nlon))
 read(ID0,*) lon

 read(ID0,*) mskfile

 close(ID0)

return
end subroutine inp_txt