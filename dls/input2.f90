subroutine input
 use COMM
 implicit none
 integer :: length
 
 length=len_trim(dls_file)
 if((dls_file(length-1:length)=='nc') .or. (dls_file(length-1:length)=='NC')) then
   print*,''
   print*,'Error: the code can ONLY read dls from ASCII file, '
   print*,'please set "NC_LIB= " in Makefile and try again ...'
   stop 'Now code stops !'
 endif

 call inp_txt

return
end subroutine input