! -----------------------------------------------------------------
! subroutines used by fclimdex_station, to calculate index: CDD, CWD.
!
! definitions:
! CDD: Maximum length of dry spell, maximum number of consecutive days with RR < 1mm.
! CWD: Maximum length of wet spell, maximum number of consecutive days with RR ≥ 1mm
!
! input
!    none
!   (transfer data through the module COMM)

     subroutine CDD
      use COMM
      use functions
      implicit none
      integer      :: year, month, day, kth, cnt,i, Ky   ! loop index and/or count #
      real         :: ocdd(YRS), ocwd(YRS), &     ! annual index to be calculated
                       nncdd, nncwd               ! count #

      if(Prcp_miss) return   ! if all PRCP missing, ignore this step


! Question: Should we set nncdd and nncwd to zero at the end/beginning of each year? !!!!!!!!!!!!!!!!!!!!!!!!!!!!
! answer by Lisa: no, we put them in the beginning year of the span
! So, is the code correct when corssing years ?????????????????????????????????????????????????
! LVA (16/8/12):  We want the spell to continue across the start of a year i.e. if it goes from 25th Dec 2005 to 5th Jan 2006 we want
!       to ensure that this is counted as a spell of 12 days and counted against 2005. Resetting should only be done for new indices CDDa   !       and CWDa

! get annual index
      cnt=0
      ocdd=0.
      ocwd=0.
      do i=1,YRS
        if(i==1)nncdd=0.
        if(i==1)nncwd=0.
        year=i+SYEAR-1
        Ky=leapyear(year)+1
        do month=1,12
             kth=mon(month,Ky)
          do day=1,kth
            cnt=cnt+1
            if(ismiss(PRCP(cnt))) then  ! CDD and CWD stops, reset the # to zero !
              if(nncwd.gt.ocwd(i)) ocwd(i)=nncwd ! from Markus
              if(nncdd.gt.ocdd(i)) ocdd(i)=nncdd ! from Markus
              nncdd=0.
              nncwd=0.
            elseif(PRCP(cnt).lt.1) then  ! CDD continues, CWD stops
              nncdd=nncdd+1.
              if(nncwd.gt.ocwd(i)) ocwd(i)=nncwd
              nncwd=0.
            else                         ! CWD continues, CDD stops
              nncwd=nncwd+1.
              if(nncdd.gt.ocdd(i)) ocdd(i)=nncdd
              nncdd=0.
            endif
          enddo  ! loop for day
        enddo    ! loop for month

        if(ocwd(i).lt.nncwd) then
          if(year.eq.EYEAR) then         ! Is it the end year?
                  ocwd(i)=nncwd
          elseif(PRCP(cnt+1).lt.1..or.ismiss(PRCP(cnt+1)))then  ! not end year, but next day is dry.
                  ocwd(i)=nncwd
                  nncwd=0.
          endif
        endif

        if(ocdd(i).lt.nncdd) then
          if(year.eq.EYEAR) then        ! Is it the end year?
                  ocdd(i)=nncdd
          elseif(PRCP(cnt+1).ge.1..or.ismiss(PRCP(cnt+1)))then  ! not end year, but next day is wet.
                  ocdd(i)=nncdd
                  nncdd=0.
          endif
          if(ocdd(i).eq.0) then
            write(ID_log,'(a20,1x,i4,a)') stnID,year,': CDD=0 !'
            ocdd(i)=MISSING  ! Why only set ocdd? answer: Should be no no-dry days, but should put error log file!
          endif
        endif

! LVA (16/8/12): Hongang need to create output logfile called CDD_log.txt etc with times when CDD missing.

! check if annual PRCP is MISSING
! if yes, set index=MISSING
        if(YNASTAT(i,1).eq.1) then
          ocdd(i)=MISSING
          ocwd(i)=MISSING
        endif

      enddo     ! loop for year

! output the index
      call save_annual(17,ocdd)

      call save_annual(18,ocwd)

      return
      end subroutine CDD 
