! --------------------------------------------
!
! If you don't want to use the Makefile,
! this version can be compiled on Windows and Linux machines....
! e.g.:  ifort all.f90 -o fclimdex.exe [openmp]
!        ./fclimdex.exe
!
 include 'modules.f90'
 include 'main.f90'
 include 'utility.f90'
 include 'CDD.f90'
 include 'FD.f90'
 include 'GSL.f90'
 include 'QC.f90'
 include 'R95p.f90'
 include 'Rnnmm.f90'
 include 'RX5day.f90'
 include 'TX10p.f90'
 include 'TXX.f90'
 include 'HeatWave.f90'
