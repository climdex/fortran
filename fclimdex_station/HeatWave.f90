! -------------------------------------------------------------------------
! To calculate Heat Wave Index proposed by Nairn, Fawcett and Ray,
! with modification by Lisa Alexander.
!  document from Lisa.
!
      subroutine Heatwave
      use COMM
      use functions 

      integer :: i,k,ID
      real,dimension(MaxYear*DoY) :: ADT, EHIx,EHIn,EHIa, EHIsx,EHIsn,EHIsa,EHFx,EHFn,EHFa

      if(Tmax_miss .or. Tmin_miss) return

      ADT  =MISSING
      EHIx =MISSING
      EHIn =MISSING
      EHIa =MISSING
      EHIsx=MISSING
      EHIsn=MISSING
      EHIsa=MISSING
      EHFx =MISSING
      EHFn =MISSING
      EHFa =MISSING

! get ADT, Average Daily Temperature
      ADT=(TMAX+TMIN)/2.
      where(TMAX==MISSING) ADT=MISSING
      where(TMIN==MISSING) ADT=MISSING


! get all EHI indices
     call get_EHI(TOT,TMAX(1:TOT),EHIx(1:TOT),EHIsx(1:TOT),EHFx(1:TOT))  ! max
     call get_EHI(TOT,TMIN(1:TOT),EHIn(1:TOT),EHIsn(1:TOT),EHFn(1:TOT))  ! min
     call get_EHI(TOT, ADT(1:TOT),EHIa(1:TOT),EHIsa(1:TOT),EHFa(1:TOT))  ! ave

! output the results.
! Note that we didn't remove first/last successive all-MISSING data yet here.
     k=40
     call getID(ID)
     open(ID,file=ofile(k))
     write(ID,'(a)') 'Year Month  Day    EHIx    EHIn    EHIa   EHIsx   EHIsn   EHIsa    EHFx    EHFn    EHFa'
     do i=1,TOT
        write(ID,'(3(i4,1x),9f8.1)') YMD(i,1),YMD(i,2),YMD(i,3),EHIx(i),EHIn(i),EHIa(i), &
                                EHIsx(i),EHIsn(i),EHIsa(i),EHFx(i),EHFn(i),EHFa(i)
     enddo
     close(ID)
    
     return
     end subroutine HeatWave


! -------------------------------------------------------------------------
!  get each EHI, EHIs, EHF index
!
   subroutine get_EHI(n,data,EHIa,EHIs,EHF)
   use COMM
   use functions
   implicit none
   integer :: n,i,j,k,indx(1),cnt
   real,dimension(n) :: data,EHIa,EHIs,EHF
   real,allocatable  :: data0(:)
   real :: T95

  ! get EHIa, acclimatisation Excess Heat Index
      i=1
      do
        call getday(n,data,32,i,k)
        if(k==0) goto 100
        EHIa(k)=sum(data(k-2:k))/3.-sum(data(k-32:k-3))/30.
        do
          k=k+1
          i=i+1
          if(k>n) goto 100
          if(nomiss(data(k))) then
            EHIa(k)=sum(data(k-2:k))/3.-sum(data(k-32:k-3))/30.
          else
            i=k+1
            goto 101
          endif
        enddo
101     continue
      enddo
100   continue   ! finish searching all data

! get T95 which is 95% of the data between BASESYEAR and BASEEYEAR..
     indx=minloc(abs(YMD(:,1)-BASESYEAR))
     k=indx(1)
     indx=minloc(abs(YMD(:,1)-BASEEYEAR-1))
     j=indx(1)-1
     cnt=j+1-k
     allocate(data0(cnt))
     data0(1:cnt)=data(k:j)
     where(data0 == MISSING) data0=0.
     T95=0.95*sum(data0)/cnt          ! Is this "cnt" the correct number?
     deallocate(data0)

! get EHIs, absolute Excess Heat Index
      i=1
      do
        call getday(n,data,3,i,k)
        if(k==0) goto 200
        EHIs(k)=sum(data(k-2:k))/3.-T95
        do
          k=k+1
          i=i+1
          if(k>n) goto 200
          if(nomiss(data(k))) then
            EHIs(k)=sum(data(k-2:k))/3.-T95
          else
            i=k+1
            goto 201
          endif
        enddo
201     continue
      enddo
200   continue   ! finish searching all data

! get EHF, which is the Excess Heat Factor
      EHF=abs(EHIa)*EHIs
      where(EHIa==MISSING) EHF=MISSING
      where(EHIs==MISSING) EHF=MISSING
      
     return
     end subroutine get_EHI


! -------------------------------------------------------------------
! This subroutine calculates the LAST index of FIRST m successive valid data,
!     starting from Ifrom.
!
    RECURSIVE subroutine getday(n,data,m,Ifrom,Iresult)
    use COMM    !,only MISSING      
    use functions
    integer :: n,m,Ifrom,Iresult,i,j
    real     :: data(n)

    Iresult=-1
    if(m+Ifrom-1 > n) then
      Iresult=0   ! end of searching
      return
    endif
    i=Ifrom
    if(count(data(i:(i+m-1)) /= MISSING) ==m ) then
      Iresult=i+m-1
      return
    endif
    do j=i+m-1,i,-1
      if(ismiss(data(j))) exit
    enddo
    call getday(n,data,m,j+1,Iresult)

    return
    end subroutine getday