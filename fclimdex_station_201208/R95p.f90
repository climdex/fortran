! ------------------------------------------------------------------
! Calculate index R85p, R95p, R99p, PRCPTOT, R85pTOT, R95pTOT, R99pTOT
!
! definitions are:
! R85p: Annual total PRCP when RR > 85p (on a wet day RR ≥ 1.0mm).
! R95p: Annual total PRCP when RR > 95p (on a wet day RR ≥ 1.0mm).
! R99p: Annual total PRCP when RR > 99p (on a wet day RR ≥ 1.0mm)
! PRCPTOT: Annual total precipitation in wet days (RR ≥ 1.0mm).
! R85pTOT=(R85p/PRCPTOT)*100.
! R95pTOT=(R95p/PRCPTOT)*100.
! R99pTOT=(R99p/PRCPTOT)*100.
!
! input
!    none
!   (transfer data through the module COMM)
!
      subroutine R95p
      use COMM
      use functions
      implicit none
      integer     :: year, month, day, kth,cnt,leng,i, Ky ! loop index and/or count #.
      real(DP)    :: p85, p95,p99    ! 85th, 95th and 99th percentile of precipitation on wet days in the base period
      real(DP),dimension(YRS):: r85out, r95out,r99out, prcpout,R85pTOT, R95pTOT, R99pTOT  ! index to be calculated
      real(DP),dimension(TOT):: prcptmp  ! hold selected PRCP on wet days in base period
      real(DP),dimension(3)  :: rlev,rtmp  ! percentile level and results

      if(Prcp_miss) return

! get wet days PRCP during base period - prcptmp
      cnt=0
      leng=0
      prcptmp=MISSING
! The year loop should be changed because it only needs to cover base years !!!!!!!!!!!!!!!!!!!!
      do i=1,YRS
        year=i+SYEAR-1
        Ky=leapyear(year)+1
        do month=1,12
          kth=Mon(month,Ky)
          do day=1,kth
            cnt=cnt+1
            if(year.ge.BASESYEAR.and.year.le.BASEEYEAR.and.	&
              nomiss(PRCP(cnt)).and.PRCP(cnt).ge.1.)then
              leng=leng+1
              prcptmp(leng)=PRCP(cnt)  ! wet days PRCP during base period
            endif
          enddo
        enddo
      enddo

! 85th, 95th and 99th percentile of precipitation on wet days in the base period
!      rlev(1)=0.95
!      rlev(2)=0.99
      rlev=(/0.85,0.95,0.99/)
      call percentile(prcptmp,leng,3,rlev,rtmp)
!      call percentile(prcptmp(1:leng),leng,2,rlev,rtmp) ! We should use this one ??????!!!!!!!!!!!!!!
      p85=rtmp(1)
      p95=rtmp(2)
      p99=rtmp(3)
!      p95=percentile(prcptmp,leng,0.95)
!      p99=percentile(prcptmp,leng,0.99)

! get R85p, R95p, R99p
      cnt=0
      r85out=0.
      r95out=0.
      r99out=0.
      prcpout=0.
      do i=1,YRS
        year=i+SYEAR-1
        Ky=leapyear(year)+1
        do month=1,12
          kth=Mon(month,Ky)
          do day=1,kth
            cnt=cnt+1
            if(PRCP(cnt).ge.1..and.nomiss(PRCP(cnt)))then
              prcpout(i)=prcpout(i)+PRCP(cnt)
!!              if(PRCP(cnt).gt.p95) r95out(i)=r95out(i)+PRCP(cnt)
!!              if(PRCP(cnt).gt.p99) r99out(i)=r99out(i)+PRCP(cnt)
! Should we move the nomiss(R95) to before the beginning of the loop?  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
              if(nomiss(p85).and.PRCP(cnt).gt.p85) r85out(i)=r85out(i)+PRCP(cnt)
              if(nomiss(p95).and.PRCP(cnt).gt.p95) r95out(i)=r95out(i)+PRCP(cnt)
              if(nomiss(p99).and.PRCP(cnt).gt.p99) r99out(i)=r99out(i)+PRCP(cnt)
            endif
          enddo
        enddo
        
! check if annual PRCP is MISSING
! if yes, set index=MISSING
        if(YNASTAT(i,1).eq.1) then
          prcpout(i)=MISSING
          r85out(i)=MISSING
          r95out(i)=MISSING
          r99out(i)=MISSING
        endif
      enddo

! get R85pTOT, R95pTOT, R99pTOT:
    R85pTOT=MISSING
    R95pTOT=MISSING
    R99pTOT=MISSING
    where(prcpout/=MISSING .and. r85out/=MISSING .and. prcpout/=0.) R85pTOT=r85out/prcpout*100.
    where(prcpout/=MISSING .and. r95out/=MISSING .and. prcpout/=0.) R95pTOT=r95out/prcpout*100.
    where(prcpout/=MISSING .and. r99out/=MISSING .and. prcpout/=0.) R99pTOT=r99out/prcpout*100.

! output the index
      call save_annual(37,r85out)
      call save_annual(19,r95out)
      call save_annual(20,r99out)
      call save_annual(21,prcpout)

      call save_annual(34,R85pTOT)
      call save_annual(35,R95pTOT)
      call save_annual(36,R99pTOT)

     if(save_thresholds) call save_thres_PRCP(39,p85,p95,p99)

      return
      end subroutine R95p 
