! -----------------------------------------------------------------
! calculate index TXx, TXn, TNx, TNn, DTR, ETR.
!
! definitions are:
! TXx, Monthly maximum value of daily maximum temperature
! TXn, Monthly minimum value of daily maximum temperature:
! TNx, Monthly maximum value of daily minimum temperature
! TNn, Monthly minimum value of daily minimum temperature
! DTR, Daily temperature range: Monthly mean difference between TX and TN
! ETR = TXx-TNn
!
! input
!    none
!   (transfer data through the module COMM)
!
      subroutine TXX
      use COMM
      use functions
      implicit none
      integer :: year,month,day,kth,cnt,nn,i,k, Ky  ! loop index and/or count #
      real    :: oout(YRS,13,4),yout(YRS,4), dtr(YRS,13), ETR(YRS,13) ! monthly/annual index to be calculated

      if(Tmax_miss .and. Tmin_miss) return

! get monthly index
      oout=MISSING
      dtr=0.
      cnt=0
      do i=1,YRS
        year=i+SYEAR-1
        Ky=leapyear(year)+1
        do month=1,12
          Kth=Mon(month,Ky)
          nn=0
          do day=1,kth
            cnt=cnt+1
            if(nomiss(TMAX(cnt)).and.nomiss(TMIN(cnt))) then
              dtr(i,month)=dtr(i,month)+(TMAX(cnt)-TMIN(cnt))
              nn=nn+1
            endif
            if(nomiss(TMAX(cnt)).and.(ismiss(oout(i,month,1)).or.  &
              TMAX(cnt).gt.oout(i,month,1))) then
              oout(i,month,1)=TMAX(cnt) ! TXX
            endif
            if(nomiss(TMAX(cnt)).and.(ismiss(oout(i,month,2)).or.  &
              TMAX(cnt).lt.oout(i,month,2))) then
              oout(i,month,2)=TMAX(cnt) ! TXN
            endif
            if(nomiss(TMIN(cnt)).and.(ismiss(oout(i,month,3)).or.  &
              TMIN(cnt).gt.oout(i,month,3))) then
              oout(i,month,3)=TMIN(cnt) ! TNX
            endif
            if(nomiss(TMIN(cnt)).and.(ismiss(oout(i,month,4)).or.  &
              TMIN(cnt).lt.oout(i,month,4))) then
              oout(i,month,4)=TMIN(cnt) ! TNN
            endif
          enddo
! check if monthly TX, TN is MISSING
! if yes, set monthly index=MISSING
!          if(nn.gt.0.and.MNASTAT(i,month,2).eq.0.and.	  &
!            MNASTAT(i,month,3).eq.0) then   ! Attention: up to 6 MISSING data allowed here, fix it !!!!!!!!!!!!!!!!!!!
          if(nn.gt.0 .and. count(TMAX(cnt-kth+1:cnt)==MISSING .or. TMIN(cnt-kth+1:cnt)==MISSING).le.3) then   ! Attention: up to 3 MISSING data allowed here..
            dtr(i,month)=dtr(i,month)/nn
          else
            dtr(i,month)=MISSING
          endif
          if(MNASTAT(i,month,2).eq.1)then
            oout(i,month,1)=MISSING
            oout(i,month,2)=MISSING
          endif
          if(MNASTAT(i,month,3).eq.1)then
            oout(i,month,3)=MISSING
            oout(i,month,4)=MISSING
          endif
        enddo  ! month loop
      enddo    ! year loop

! get annual index
      yout=MISSING
      do i=1,YRS
        nn=0
        do month=1,12
          if(nomiss(oout(i,month,1)).and.(ismiss(yout(i,1)).or.	 &
            oout(i,month,1).gt.yout(i,1))) then
            yout(i,1)=oout(i,month,1)
          endif
          if(nomiss(oout(i,month,2)).and.(ismiss(yout(i,2)).or.	 &
            oout(i,month,2).lt.yout(i,2))) then
            yout(i,2)=oout(i,month,2)
          endif
          if(nomiss(oout(i,month,3)).and.(ismiss(yout(i,3)).or.	 &
            oout(i,month,3).gt.yout(i,3))) then
            yout(i,3)=oout(i,month,3)
          endif
          if(nomiss(oout(i,month,4)).and.(ismiss(yout(i,4)).or.	 &
            oout(i,month,4).lt.yout(i,4))) then
            yout(i,4)=oout(i,month,4)
          endif
          if(nomiss(dtr(i,month))) then
            dtr(i,13)=dtr(i,13)+dtr(i,month)
            nn=nn+1
          endif
        enddo
        
! check if annual TX, TN is MISSING
! if yes, set annual index=MISSING
        if(nn.gt.0.and.YNASTAT(i,2).eq.0.and.YNASTAT(i,3).eq.0) then
          dtr(i,13)=dtr(i,13)/nn
        else
          dtr(i,13)=MISSING
        endif
        if(YNASTAT(i,2).eq.1) then
          yout(i,1)=MISSING
          yout(i,2)=MISSING
        endif
        if(YNASTAT(i,3).eq.1) then
          yout(i,3)=MISSING
          yout(i,4)=MISSING
        endif
      enddo

    oout(:,13,:)=yout(:,:)   ! annual index

! calculate ETR:
    ETR=MISSING
    where(oout(:,:,1)/=MISSING .and. oout(:,:,4)/=MISSING) ETR(:,:)=oout(:,:,1)-oout(:,:,4)

! output the index

    if(.not. Tmax_miss) then
      do k=1,2
        call save_monthly(k+5,oout(:,:,k))
      enddo
    endif

    if(.not. Tmin_miss) then
      do k=3,4
        call save_monthly(k+5,oout(:,:,k))
      enddo
    endif

    if((.not. Tmax_miss) .and. (.not. Tmin_miss)) then
       call save_monthly(10,dtr)
       call save_monthly(33,ETR)
    endif

    return
    end subroutine TXX 