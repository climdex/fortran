! ----------------------------------------------------------------
! get index R10mm, R20mm, Rnnmm, SDII
!
! definitions are:
! R10mm: Annual count of days when PRCP≥ 10mm.
! R20mm: Annual count of days when PRCP≥ 20mm.
! Rnnmm: Annual count of days when PRCP≥ nnmm, nn is a user defined threshold.
! SDII:  Simple pricipitation intensity index (on wet days, w PRCP ≥ 1mm).
!
! input
!    none
!   (transfer data through the module COMM)
!
     subroutine Rnnmm
      use COMM
      use functions !, only : leapyear
      integer      :: year,month,day,kth,cnt,nn,i,k, Ky ! loop index and/or count #.
      real         :: oout(YRS,3),sdii(YRS)  ! annual index to be calculated

      if(Prcp_miss) return

! get the annual index
      cnt=0
      oout=0.
      sdii=0.
      do i=1,YRS
        nn=0
        year=i+SYEAR-1
        Ky=leapyear(year)+1
        do month=1,12
          Kth=Mon(month,Ky)
          do day=1,kth
            cnt=cnt+1
            if(PRCP(cnt).ge.1.) then
              sdii(i)=sdii(i)+PRCP(cnt)
              nn=nn+1
            endif
            if(PRCP(cnt).ge.10.) oout(i,1)=oout(i,1)+1.
            if(PRCP(cnt).ge.20.) oout(i,2)=oout(i,2)+1.
            if(PRCP(cnt).ge.PRCPNN) oout(i,3)=oout(i,3)+1.
          enddo
        enddo
        if(nn.gt.0) then
          sdii(i)=sdii(i)/nn
        endif
      enddo  ! loop for year

! If annual PRCP is MISSING, also set annual index=MISSING
      do i=1,YRS
        if(YNASTAT(i,1).eq.1) then
           oout(i,:)=MISSING
           sdii(i)=MISSING
        endif
      enddo

! output the index
      do k=1,3
        call save_annual(10+k,oout(:,k))
      enddo

      call save_annual(14,sdii)

      return
      end subroutine Rnnmm 
