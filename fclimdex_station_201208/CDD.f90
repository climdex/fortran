! -----------------------------------------------------------------
! subroutines used by fclimdex_station, to calculate index: CDD, CWD.
!
! definitions:
! CDD: Maximum length of dry spell, maximum number of consecutive days with RR < 1mm.
! CWD: Maximum length of wet spell, maximum number of consecutive days with RR ≥ 1mm
! CDDA annual CDD with no year end extension
! CWDA annual CWD with no year end extension
!
! input
!    none
!   (transfer data through the module COMM)

     subroutine CDD
      use COMM
      use functions
      implicit none
      integer      :: year, month, day, kth, cnt,i, Ky   ! loop index and/or count #
      real         :: ocdd(YRS), ocwd(YRS), &            ! annual index to be calculated
                      CDDA(YRS),CWDA(YRS), &             ! annual index to be calculated
                      nncdd, nncwd,ncdda,ncwda           ! count #

      if(Prcp_miss) return   ! if all PRCP missing, ignore this step


! Question: Should we set nncdd and nncwd to zero at the end/beginning of each year? !!!!!!!!!!!!!!!!!!!!!!!!!!!!
! answer by Lisa: no, we put them in the beginning year of the span
! So, is the code correct when corssing years ?????????????????????????????????????????????????

! get annual index
      cnt=0
      ocdd=0.
      ocwd=0.
      do i=1,YRS
        ncdda=0.
        ncwda=0.
        if(i==1)nncdd=0.
        if(i==1)nncwd=0.
        year=i+SYEAR-1
        Ky=leapyear(year)+1
        do month=1,12
             kth=mon(month,Ky)
          do day=1,kth
            cnt=cnt+1
            if(ismiss(PRCP(cnt))) then  ! CDD and CWD stops, reset the # to zero !
              if(nncwd.gt.ocwd(i)) ocwd(i)=nncwd ! from Markus
              if(nncdd.gt.ocdd(i)) ocdd(i)=nncdd ! from Markus
              if(ncdda>cdda(i)) cdda(i)=ncdda
              if(ncwda>cwda(i)) cwda(i)=ncwda
              ncdda=0.
              ncwda=0.
              nncdd=0.
              nncwd=0.
            elseif(PRCP(cnt).lt.1) then  ! CDD continues, CWD stops
              nncdd=nncdd+1.
              ncdda=ncdda+1
              if(nncwd.gt.ocwd(i)) ocwd(i)=nncwd
              if(ncdda>cdda(i)) cdda(i)=ncdda
              nncwd=0.
              ncwda=0.
            else                         ! CWD continues, CDD stops
              nncwd=nncwd+1.
              ncwda=ncwda+1.
              if(nncdd.gt.ocdd(i)) ocdd(i)=nncdd
              if(ncdda.gt.cdda(i)) cdda(i)=ncdda
              nncdd=0.
              ncdda=0.
            endif
          enddo  ! loop for day
        enddo    ! loop for month

        if(ocwd(i).lt.nncwd) then
          if(year.eq.EYEAR) then         ! Is it the end year?
                  ocwd(i)=nncwd
          elseif(PRCP(cnt+1).lt.1..or.ismiss(PRCP(cnt+1)))then  ! not end year, but next day is dry.
                  ocwd(i)=nncwd
                  nncwd=0.
          endif
        endif

        if(ocdd(i).lt.nncdd) then
          if(year.eq.EYEAR) then        ! Is it the end year?
                  ocdd(i)=nncdd
          elseif(PRCP(cnt+1).ge.1..or.ismiss(PRCP(cnt+1)))then  ! not end year, but next day is wet.
                  ocdd(i)=nncdd
                  nncdd=0.
          endif
          if(ocdd(i).eq.0) then
            write(ID_log,'(a20,1x,i4,a)') stnID,year,': CDD=0 !'
            ocdd(i)=MISSING  ! Why only set ocdd? answer: Should be no no-dry days, but should put error log file!
          endif
        endif

! check if annual PRCP is MISSING
! if yes, set index=MISSING
        if(YNASTAT(i,1).eq.1) then
          ocdd(i)=MISSING
          ocwd(i)=MISSING
          cdda(i)=MISSING
          cwda(i)=MISSING
        endif

      enddo     ! loop for year

! output the index
      call save_annual(17,ocdd)

      call save_annual(18,ocwd)

      call save_annual(41,cdda)

      call save_annual(42,cwda)


      return
      end subroutine CDD 
