! subroutine used by dls_1.1.f90
!
! used to output dls values (after interpolation) in ASCII format.
! also contains the lat and lon dimensions
! and grid file name
! to be used by gridding project.
!
! input:
!      none
!   (transfer data through the module COMM)
!
subroutine out_txt
 use COMM
 integer       :: ID, &    ! free unit ID for I/O
                  status   ! whether it's successful to create output file
 character*250 :: out_file ! the file to save the dls values

out_file=trim(outfile)//'.txt'

 call getID(ID)
open(ID,file=out_file,IOstat=status)
if(status/=0) then
  print*, 'Error: can NOT create the desired file for output: '
  print*,trim(out_file)
  stop 'Now the code stops ...'
endif

write(ID,'(i4,a)') N_month,' monthly/annual values,'
write(ID,'(i4,a)') Nlat,' latitude values,'
write(ID,'(i5,a)') int(dls_default),' is default/minimum dls value.'
write(ID,'(10x,<N_month>(a5,2x))') (monthnames(i-1),i=monthf,13)

do i=1,Nlat
  write(ID,'(f7.3,2x,<N_month>(i6,1x))') lat(i),(dlss(1,i,j),j=1,N_month)
enddo

write(ID,'(i4,a)') Nlon,' longitude values:'
write(ID,*) lon(1:Nlon)

write(ID,'(a,a)') trim(grid_file),'  is the mskfile (grid_file)...'
 close(ID)

return
end subroutine out_txt