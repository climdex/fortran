  NSTD  BYear EYear PRCPNN         data names for Tmax, Tmin and PRCP 
   4    1961  1990   25       NCEP_PR_1990-2000.nc    NCEP_TN_1990-2000.nc   NCEP_TX_1990-2000.nc 
   4    1961  1990   25  ! AWAP_TN_1911-2009_1deg   AWAP_TX_1911-2009_1deg,    	AWAP_PRCP_1900-2009_1deg
   4    1961  1990   25 !AWAP_PRCP_1900-2009_1deg ,    AWAP_TX_1911-2009_1deg,    AWAP_TN_1911-2009_1deg.nc


   4    1961  1990   25   ! Data names can be in any order, you can separate data names with blanks, tabulars or ','s, and comment with "!"
   4    1961  1990   25   ! It's OK without ".nc".